#loads packages, functions, raw data and necessary lookups
source("R/01_loads_libs.R")
source("R/02_loads_data.R")

#sets chart arguments for Newsworthy-style charts
source("R/03_sets_chart_arguments.R")

#cleans raw data
source("cleaning/04_cleans_data.R")

#calculates pre-corona average and change against it
source("analysis/05_calculates_change.R")

#creates data for robot writer
source("analysis/06_saves_data_for_robot_writer.R")

#graphics
source("analysis/07_regional_line_charts.R") #national and regional charts
source("analysis/08_sm_regional_line_chart.R") #sm and region-by-region charts
source("analysis/09_makes_treemap.R") #treemaps showing waits by speciality

#uploads charts to aws
source("analysis/10 _uploads_charts_to_aws.R")

