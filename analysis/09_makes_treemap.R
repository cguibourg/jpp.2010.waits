#this creates treemaps for each region, showing the total number of waits by speciality

#list of desired regions to loop over
plot.regions <- unique(plot.data$name)

#defines treemap function
create_regional_tree_maps <- function(region){
  
  #region <- plot.regions[1]
  
  #specifies region long name for filepath
  region.chart.name <- data.with.average %>%
    filter(name == region) %>%
    select(id) %>%
    unique() %>%
    unlist() %>%
    unname()
  
  #grabs swedish name of latest month
  month <- data %>% filter(date == max_date) %>% select(month_swe) %>% unique() %>% unlist() %>% unname()
  
  #grabs latest month's total waits and rounds to nearest 100
  monthly_total <- data %>%
    filter(name == region) %>%
    filter(date == max_date) %>%
    select(short_title,
           value = cases_total) %>%
    filter(short_title == "Totalt") %>%
    select(value) %>%
    round(-2) %>%
    unlist() %>% 
    unname()
  
  #filter for: regional figures, latest figures
  monthly.regional.total.by.speciality <- data %>%
    filter(name == region) %>%
    filter(date == max_date) %>%
    select(short_title,
           value = cases_total) %>%
    filter(short_title != "Totalt")
  
  #creates treemap
  treemap <- ggplot(data = monthly.regional.total.by.speciality,
                    aes(area = value,
                        fill = short_title)) +
    geom_treemap(show.legend = F,
                 colour = "white") +
    geom_treemap_text(aes(label = paste0(short_title, "\n", format(round(value, -1), big.mark = " "))),
                      family = "OpenSans-Regular",
                      colour = "#0F1108") +
    theme_jpp() +
    scale_fill_manual(values = categorical.15.colour.scale) +
    labs(title = "Operationerna med flest som väntar",
         subtitle = paste0("Så här fördelas de totalt cirka ", format(monthly_total, big.mark = " "), " som väntar på operation i ", region, "\ni ", month))
  
  #saves to dropbox
  finalise_plot(treemap,
                source_name = "Källa: Väntetider i vården, SKR",
                save_filepath = paste0("~/Dropbox/Newsworthy/data/jpp.2010.waits/output/waits--treemap_by_speciality--",
                                       region.chart.name,
                                       "--", 
                                       trigger.date,
                                       ".png"),
                footnote = "Siffrorna är avrundade och kategorin 'övrigt' har inte tagits med",
                height_pixels = 2000)
}

#loops treemap function over all regions
lapply(plot.regions,
       create_regional_tree_maps)
