#this defines a function to make a line chart for a specific region, comparing 2020-21 waits
#with typical level for that month. 
#the function then loops over all regions and saves charts out to dropbox

#list of all desired region names
plot.regions <- unique(plot.data$name)

#defines line chart function
create_regional_line_charts <- function(region, font, font.bold, colour, type, subtitle){
  
  #region <- "Stockholm"
  
  #filters full data for desired region
  regional.plot.data <- plot.data %>%
    filter(name == region)
  
  #sets unique identifier of filepath name that chart will be saved to 
  if(unique(regional.plot.data$type) == "Land"){
    filepath.name <- "Sverige"
  } else {
    filepath.name <- unique(regional.plot.data$id)
  }
  
  #grabs latest month's gap between existing waits and typical waits
  latest.diff <- regional.plot.data %>%
    filter(date == max_date) %>%
    mutate(latest_diff = perc_over_90 - average_monthly_perc_over_90_rate_15_19) %>%
    select(latest_diff) %>%
    unlist() %>%
    unname()
  
  #sets name of region for plot title (sverige instead of sweden)
  if(unique(regional.plot.data$type) == "Land"){
    region.label <- "Sverige"
  } else {
    region.label <- region
  }
  
  #creates plot
  regional.overall.plot <- ggplot(data = regional.plot.data,
                                  aes(x = date,
                                      y = perc_over_90)) +
    coord_cartesian(clip = "off") +
    #this draws an background area plot for 2020-2021 figures
    geom_area(data = regional.plot.data %>% filter(date >= as.Date("2020-01-01")),
              aes(x = date,
                  y = perc_over_90),
              fill = colour,
              colour = NA,
              alpha = 0.2) +
    #this draws an background area plot for average figures
    geom_area(data = regional.plot.data %>% filter(date >= as.Date("2020-01-01")),
              aes(x = date,
                  y = average_monthly_perc_over_90_rate_15_19),
              fill = "white",
              colour = NA,
              alpha = 1) +
    #this is a hacky way of adding in gridlines when the default ones get covered up by the area geom
    geom_hline(yintercept = seq(0, 100, 25),
               size = 0.1,
               colour = light.grey) +
    #adds lines for 2020-21 and average waits
    geom_line(data = regional.plot.data,
              aes(x = date,
                  y = average_monthly_perc_over_90_rate_15_19),
              colour = dark.grey,
              size = 1.25,
              alpha = 0.8) +
    geom_line(data = regional.plot.data %>% filter(year >= 2020),
              colour = colour,
              alpha = 0.9,
              size = 1.25) +
    #horizontal baseline
    geom_hline(yintercept = 0,
               size = 0.5,
               colour = black) +
    geom_point(data = regional.plot.data %>% filter(year >= 2020 & date == max_date),
               aes(x = date,
                   y = average_monthly_perc_over_90_rate_15_19),
               colour = dark.grey,
               fill = "white",
               shape = 21,
               size = 2.5,
               inherit.aes = F,
               stroke = 1.5) +
    #adds in point and label for earliest and latest month
    geom_point(data = regional.plot.data %>% filter(year == 2020 & grepl("-01-",date)),
               aes(x = date,
                   y = average_monthly_perc_over_90_rate_15_19),
               colour = dark.grey,
               fill = "white",
               inherit.aes = F,
               size = 2) +
    geom_point(data = regional.plot.data %>% filter(year == 2020 & grepl("-01-",date)),
               colour = colour,
               fill = "white",
               size = 2) +
    geom_point(data = regional.plot.data %>% filter(year >= 2020 & date == max_date),
               colour = colour,
               fill = "white",
               shape = 21,
               size = 2.5,
               stroke = 1.5) +
    geom_text(data = regional.plot.data %>% filter(year >= 2020 & date == max_date),
              aes(label = paste0(round(perc_over_90), " %")),
              colour = colour,
              size = 4,
              hjust = 0,
              nudge_x = 10,
              family = font) +
    geom_text(data = regional.plot.data %>% filter(year >= 2020 & date == max_date),
              aes(y = average_monthly_perc_over_90_rate_15_19,
                  label = paste0(round(average_monthly_perc_over_90_rate_15_19), " %")),
              colour = dark.grey,
              size = 4,
              hjust = 0.1,
              nudge_x = 10,
              family = font) +
    #sets scales
    scale_x_date(limits = c(as.Date("2020-01-01"),
                            as.Date(trigger.date + 30)),
                 date_breaks = "2 months",
                 labels = function(x) ifelse(lubridate::month(x) %in% c(1, 2),
                                             paste0(tolower(lubridate::month(x, label = T)), " ", lubridate::year(x)),
                                             tolower(lubridate::month(x, label = T)))) +
    scale_y_continuous(labels = function(x) paste0(x, " %")) +
    #sets custom NW theme
    theme_jpp() +
    theme(panel.grid.major.y = element_blank(),
          plot.subtitle = element_markdown(lineheight = 1.2)) +
    #sets labels
    labs(title = ifelse(latest.diff > 5,
                        "Fler tvingas vänta för länge under corona",
                        paste0("Relativt små förändringar i ", region.label)),
         subtitle = paste0("Andel patienter som tvingas vänta längre än 90 dagar på operation eller åtgärd<br>varje månad i ", region.label, ", under ", subtitle))
  
  regional.overall.plot  
  
  #saves plot
  finalise_plot(regional.overall.plot,
                source_name = "Källa: Väntetider i vården, SKR",
                save_filepath = paste0("~/Dropbox/Newsworthy/data/jpp.2010.waits/output/waits--overall_by_region--", 
                                       filepath.name, 
                                       "--", 
                                       trigger.date, 
                                       type,
                                       ".png"))
  
}

#newsworthy-style plots
lapply(plot.regions,
       create_regional_line_charts,
       nw.font,
       nw.font.bold,
       nw.colour,
       "",
       subtitle = paste0("<b style='color:",
                         nw.colour,
                         "; font-family:",
                         nw.font.bold.label,
                         "'>2020-2021</b> och <b style='color:",
                         dark.grey,
                         "; font-family:",
                         nw.font.bold.label,
                         "'>genomsnittet 2015-2019</b>"))
