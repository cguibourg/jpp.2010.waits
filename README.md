## Vad är detta? ##
Vi gör en månadsvis bevakning av väntetider i vården under coronapandemin, utifrån SKR:s siffror.

## Vad ska jag göra? ##
Jens har en scraper för deras sajt, när det finns nya siffror publicerade kör han denna och sparar nya datan till NW-databasen. 

Skripten i R-projektet hämtar in nya datan, bearbetar till rapporter samt gör grafer. 

Det bör funka att köra skriptet `00_run_all.R` för att köra igenom allt (om du har alla R-paket som specificeras i `/R/01_loads_libs.R`). Då ska ny data vara utsparad till Dropbox (`/data/jpp.2010.waits/`) och nya grafer uppladdade. 

Generera nya rapporter utifrån nya datan (Kolla avsnittet “Robot Reporter” i [dokumentationen](https://docs.google.com/document/d/1EUncT-jotxNXRXTUvNOaH2gTCUhNkVfDgiqPyY6SQgk/edit#)) och gå in på hemsidan för att kolla att de ser bra ut och publicera.
