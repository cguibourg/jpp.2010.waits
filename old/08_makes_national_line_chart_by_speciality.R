#calculates average month by month 2015-2019
average.by.month <- data %>%
  filter(year >= 2015 & year < 2020) %>%
  group_by(name,
           short_title, 
           month) %>%
  summarise(monthly_wait_over_90 = sum(cases_over_90, na.rm = T),
            monthly_total_count = sum(cases_total, na.rm = T)) %>%
  mutate(average_monthly_perc_over_90_rate_15_19 = 100 * monthly_wait_over_90 / monthly_total_count) 

plot.data <- data %>%
  filter(type == "Land" & short_title != "Totalt") %>%
  filter(!is.na(month)) %>%
  filter(year == max_year) %>%
  mutate(fake_date = as.Date(paste0("2019-", month, "-01"))) %>%
  left_join(average.by.month) %>%
  mutate(short_title = gsub("(.{7,}?)\\s", "\\1\n", short_title))

month.swe.abbr <- substr(unique(data.for.csv$month_swe), 1, 3)

creates_sm_plot <- function(font, logo, theme, colour, type, subtitle){
  
  regional.sm.overall.plot <- ggplot(data = plot.data,
                                     aes(x = fake_date,
                                         y = perc_over_90,
                                         group = year)) +
    geom_area(data = plot.data %>% filter(fake_date >= as.Date("2019-03-01")),
              aes(x = fake_date,
                  y = perc_over_90),
              fill = colour,
              colour = NA,
              alpha = 0.2) +
    geom_area(data = plot.data %>% filter(fake_date >= as.Date("2019-03-01")),
              aes(x = fake_date,
                  y = average_monthly_perc_over_90_rate_15_19),
              fill = "white",
              colour = NA,
              alpha = 1) +
    geom_hline(yintercept = seq(0, 100, 25),
               size = 0.1,
               colour = "#dedede") +
    geom_line(data = plot.data,
              aes(x = fake_date,
                  y = average_monthly_perc_over_90_rate_15_19),
              colour = "#42404F",
              size = 1,
              alpha = 0.8) +
    geom_line(data = plot.data %>% filter(year == 2020),
              colour = colour,
              alpha = 0.9,
              size = 1) +
    geom_hline(yintercept = 0,
               size = 0.3,
               colour = "#333333") +
    facet_wrap(~short_title,
               ncol = 4) +
    theme_jpp() +
    theme +
    theme(panel.grid.major.y = element_blank(),
          plot.subtitle = element_markdown(lineheight = 1.2)) +
    scale_x_date(breaks = c(as.Date("2019-01-15"), as.Date(trigger.date) - 365),
                 labels = c("  jan",
                            paste0(month.swe.abbr, "  "))) +
    scale_y_continuous(labels = function(x) paste0(x, " %")) +
    labs(title = "Stora skillnader mellan olika vårdtyper",
         subtitle = subtitle)
  
  finalise_plot(regional.sm.overall.plot,
                source_name = "Källa: Väntetider i vården, SKR",
                save_filepath = paste0("~/Dropbox/Newsworthy/data/jpp.2010.waits/output/waits--overall_by_speciality_sm",
                                       "--", 
                                       trigger.date,
                                       type,
                                       ".png"),
                logo = logo,
                height_pixels = 2500)
}

creates_sm_plot(nw.font,
                nw.logo,
                         nw.theme,
                         nw.colour,
                         "",
                         subtitle = paste0("Andel patienter som tvingas vänta längre än 90 dagar på operation/åtgärd,<br>per vårdtyp under <b style='color:#5aa69d; font-family:OpenSans-Bold'>2020</b> jämfört med <b style='color:#42404F; font-family:OpenSans-Bold'>genomsnittet 2015-2019</b>"))

